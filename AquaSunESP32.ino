#include <Time.h>
#include <TimeLib.h>
#include <DS3232RTC.h>
#include <WiFiManager.h>
#include <WiFi.h>
#include <ping.h>
#include <ESPmDNS.h>
#include <HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include "SSD1306.h"
#include <DNSServer.h>
#include <WebServer.h>

#include <HashMap.h>
#include <Scheduler.h>
#include <Chronos.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <math.h>
//#include <ESP8266TrueRandom.h>
//#include <sendemail.h>
#include <Adafruit_PWMServoDriver.h>
#include "config.h"
#include "variaveis.h"
#include "ota.h"
#include "displayOledSSD1306.h"
#include "images.h"
#include "desenhos.h"
#include "mensagens.h"
#include "termometro.h"
#include "ntp.h"
#include "time.h"
#include "firebase.h"
#include "pca9685.h"
#include "efeitos.h"






void configModeCallback (WiFiManager *myWiFiManager) {
  println_dbg("Entered config mode");
  println_dbg(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  println_dbg(myWiFiManager->getConfigPortalSSID());
  setupOled();
  mensagemNenhumaRedeEncontrada();
  delay(4000);
  mensagemModoReconfiguracao();
  //atualizarIconeModoOled()
}

void setup() {

  Serial.begin(115200);
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  WiFi.setHostname(HOSTNAME_DEFAULT);
  wifiManager.setConfigPortalTimeout(180);

  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("AquaSun")) {
    println_dbg("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.restart();
    delay(1000);
  }



  //if you get here you have connected to the WiFi
  println_dbg("conectado...");
  if (!MDNS.begin(HOSTNAME_DEFAULT)) {
    println_dbg("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  println_dbg("mDNS responder started");
  setupOTA();
  estadoWifi = 1;
  setupOled();
  mensagemIniciarWLAN();
  mensagemEnderecoIP();
  delay(4000);
  setupCoordenadasOled(setupTermometro());

  display.clear();

  ///////////////////////////////

  ///////////////////////////////
  setupPersistencia();
  setupPCA9685();

  // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 80);

  printf_dbg("\n\n");

  pinMode(BUTTONPIN, INPUT_PULLUP);
  pinMode(OTAPIN, INPUT_PULLUP);

  delay(2000);

}

/*########################LOOP#########################*/
void loop() {
  buttonOTAState = digitalRead(OTAPIN);

  //If button pressed...
  if (buttonOTAState == HIGH) {

    if (!OTA_ENABLED) {
      ArduinoOTA.begin();
      println_dbg("OTA Ready");
      OTA_ENABLED = true;
    }
    println_dbg("OTA handle");
    ArduinoOTA.handle();
  }
  //simbolo wifi
  if ( WiFi.status() == WL_CONNECTED ) {
    if ( estadoWifi == 2) {
    } else if ( estadoWifi == 0) {
      if (verificarInternet())
        estadoWifi = 1;
      else
        estadoWifi = 2;
    }
  } else {
    estadoWifi = 0;
  }

  if (estadoWifiAtual != estadoWifi) {
    atualizarIconeWIFI();
  }

  if (timeProvider == 1 & estadoWifi == 2 & agendadorMudarProvider == 0) {
    mudarProvider.schedule(setupTime, 1800000);
    agendadorMudarProvider = 1;
  }

  buttonState = digitalRead(BUTTONPIN);

  //If button pressed...
  if (buttonState == LOW) {
    println_dbg("Reset acionado");
    resetDefaults();
  }

  if (!modoSetup) {
    if (temporizador == 1) {
      temporizadorAgendador.update();
      temporizadorPreset.update();
    }
    if (hasStorm || hadStorm) {
      if (!hadFadeIn) {
        atualizarOled();
        fadeIn();
        hadFadeIn = true;

      }
      if (millis() - waitTime > lastTime)  // time for a new flash
      {
        doStorm();
      }


      if (!hadFadeOut && hadStorm) {
        atualizarOled();
        fadeOut();
        hasStorm = false;
        hadFadeIn = false;
        hadStorm = false;
      }

    }

  }

  if (agendadorMudarProvider == 1) {
    mudarProvider.update();
  }

  sensors.requestTemperatures();
  verificarTemperatura(insideThermometer);//Variável tempC é modificada aqui...
  //Método para exibir gráficos dos leds com termômetro
  if (tempC > 0 && tempC != temperaturaAtual) {
    displayTemperatura(tempC);
    temperaturaAtual = tempC;
    if (temperaturaAtual < tempMin && !alertaTemp) {
      println_dbg("Alerta Temperatura Minima");
      println_dbg(temperaturaAtual);
      /*if(emailLigado==true){
        println_dbg("Alerta - Email");
        SendEmail e(smtp, smtpPort, login, password, 5000, sslLigado);
        e.send(sender, email, "nossl", "message");
        }*/
      if (regIdLigado == true) {
        println_dbg("Alerta - Push");
        doit("Alerta Temperatura Minima", String(temperaturaAtual));
      }
      alertaTemp = true;
    } else if (temperaturaAtual > tempMax && !alertaTemp) {
      println_dbg("Alerta Temperatura Maxima");
      println_dbg(temperaturaAtual);
      /*if(emailLigado==true){
        println_dbg("Alerta - Email");
         SendEmail e(smtp, smtpPort, login, password, 5000, sslLigado);
         e.send(sender, email, "nossl", "message");
        }*/
      if (regIdLigado == true) {
        println_dbg("Alerta - Push");
        doit("Alerta Temperatura Maxima", String(temperaturaAtual));
      }
      alertaTemp = true;
    } else if (temperaturaAtual <= tempMax && temperaturaAtual >= tempMin && alertaTemp) {
      alertaTemp = false;
    }
  }/* else if (tempC <= 0 && tempC != temperaturaAtual) {
    //atualizarIconeModoOled();
    //atualizarIconePreset();
    limparAreaGraficoLeds(xLed, tamYLed, tamXLimparAreaLed, tamYLimparAreaLed);//ATUALIZADO
    desenharGraficoLeds(xLed, yLed, tamXLed, tensaoA, "A");//ATUALIZADO
    desenharGraficoLeds(xLed + espacoXLed, yLed, tamXLed, tensaoB, "B");//ATUALIZADO
    desenharGraficoLeds(xLed + (espacoXLed * 2), yLed, tamXLed, tensaoC, "C");//ATUALIZADO
    temperaturaAtual = tempC;
  }*/
  server.handleClient();
}
/*########################OlED#########################*/
void atualizarOled() {
  tensaoA = map(tensaoAtual[0], 0, 1024, 0, 50);
  tensaoB = map(tensaoAtual[1], 0, 1024, 0, 50);
  tensaoC = map(tensaoAtual[2], 0, 1024, 0, 50);
  if (firstRun) {
    sensors.requestTemperatures();
    verificarTemperatura(insideThermometer);//Variável tempC é modificada aqui...
    //Método para exibir gráficos dos leds com termômetro
    if (tempC > 0) {
      desenharTermometro();
    }
    firstRun = false;
  }
  atualizarIconeWIFI();
  if (tempC > 0) {
    displayTemperatura(tempC);
  }
  if (!hasStorm) {
    limparAreaGraficoLeds(xLed, tamYLed, tamXLimparAreaLed, tamYLimparAreaLed);//ATUALIZADO
    desenharGraficoLeds(xLed, yLed, tamXLed, tensaoA, "A");//ATUALIZADO
    desenharGraficoLeds(xLed + espacoXLed, yLed, tamXLed, tensaoB, "B");//ATUALIZADO
    desenharGraficoLeds(xLed + (espacoXLed * 2), yLed, tamXLed, tensaoC, "C"); //ATUALIZADO
  } else {
    limparAreaGraficoLeds(xLed, tamYLed, tamXLimparAreaLed, tamYLimparAreaLed);//ATUALIZADO
    desenharStorm();
  }

  atualizarIconeModoOled();//ATUALIZADO
  atualizarIconePreset();
  //////////////////////////////////////////

}

void atualizarIconePreset() {
  if (temporizador == 1 && modoSetup == 0) {
    //limparAreaFundoPreset(tamXLimparAreaPreset, tamYLimparAreaPreset, rPrst);//mesmo valor do temporizador//ATUALIZADO
    ///////ATUALIZADO NA VERSÃO 18/////////
    limparAreaFundoPresetIMG(tamXLimparAreaPresetIMG, tamYLimparAreaPresetIMG);
    if (!modoSetup)
      desenharFundoPreset(xPrst, yPrst, agendador.nome);//mesmo valor do temporizador//ATUALIZADO
    else
      //limparAreaFundoPreset(tamXLimparAreaPreset, tamYLimparAreaPreset, rPrst);//mesmo valor do temporizador//ATUALIZADO
      ///////ATUALIZADO NA VERSÃO 18/////////
      limparAreaFundoPresetIMG(tamXLimparAreaPresetIMG, tamYLimparAreaPresetIMG);
  }
}

void atualizarIconeModoOled() {
  limparAreaControles(xCtrl, yCtrl,  manual_width + 1,  manual_height + 1);//ATUALIZADO ver 21
  if (!modoSetup) {
    if (temporizador) {
      desenharControleTemporizado(xCtrl, yCtrl);//ATUALIZADO

    } else {
      desenharControleManual(xCtrl, yCtrl);//ATUALIZADO
    }
  } else {
    desenharModoConfig(xCtrl, yCtrl);//ATUALIZADO
  }
}

void atualizarIconeWIFI() {
  limparAreaSinalWIFI(xWifi, yWifi, sinal_WIFI_width, sinal_WIFI_height);//ATUALIZADO
  if (estadoWifi == 1) {
    desenharSinalWIFI(xWifi, yWifi);//ATUALIZADO
  } else if (estadoWifi == 0) {
    //desenho com internet
    desenharNOSinalWIFI(xWifi, yWifi);//ATUALIZADO
  } else if (estadoWifi == 2) {
    //desenho com internet
    desenharNOInternet(xWifi, yWifi);//ATUALIZADO
  }
  estadoWifiAtual = estadoWifi;
}

/*########################RESET#########################*/
void resetDefaults() {
  delay(3000);
  WiFi.disconnect();
  gravarTemporizador("{\"timer\":\"0\"}");
  gravarStorm("{\"storm\":\"0\",\"mode\":\"1\"}");
  gravarUtc("{\"utc\":\"-3\"}");
  //gravarEmail("{\"email\":\"\",\"emailLigado\": \"0\"}");
  gravarRegId("{\"regId\":\"\",\"regIdLigado\": \"0\"}");
  gravarAutorizacao("{\"autorizacao\":\"0\"}");
  gravarTempAlerta("{\"tempMin\": 20,\"tempMax\": 30}");
  gravarPreset("{\"nome\":\"preset1\",\"led1Tensao\":900,\"led2Tensao\":300,\"led3Tensao\":500}");
  gravarPreset("{\"nome\":\"preset2\",\"led1Tensao\":500,\"led2Tensao\":500,\"led3Tensao\":1000}");
  gravarPreset("{\"nome\":\"preset3\",\"led1Tensao\":100,\"led2Tensao\":900,\"led3Tensao\":100}");
  gravarPreset("{\"nome\":\"preset4\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset5\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset6\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset7\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset8\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset9\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset10\",\"led1Tensao\":00,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset11\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"preset12\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0}");
  gravarPreset("{\"nome\":\"presetManual\",\"led1Tensao\":100,\"led2Tensao\":300,\"led3Tensao\":500}");
  gravarAgendador("[{\"hrInicio\":0,\"hrFinal\":28800000,\"nome\":\"preset1\",\"gradual\":1},{\"hrInicio\":28800000,\"hrFinal\":57600000,\"nome\":\"preset2\",\"gradual\":1},{\"hrInicio\":57600000,\"hrFinal\":0,\"nome\":\"preset3\",\"gradual\":1}]");
  ESP.restart();
}


