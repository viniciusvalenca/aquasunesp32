#include <SPI.h>
#include <FS.h>
#include "SPIFFS.h"
#include "file.h"
#include "webServer.h"
#include <ArduinoJson.h>




void setupPersistencia() {

  ledcAttachPin(PinBaseTransistor0, 1); // assign RGB led pins to channels
  ledcAttachPin(PinBaseTransistor1, 2);
  ledcAttachPin(PinBaseTransistor2, 3);
  
  // Initialize channels 
  // channels 0-15, resolution 1-16 bits, freq limits depend on resolution
  // ledcSetup(uint8_t channel, uint32_t freq, uint8_t resolution_bits);
  ledcSetup(1, 1000, 10); // 12 kHz PWM, 8-bit resolution
  ledcSetup(2, 1000, 10);
  ledcSetup(3, 1000, 10);

  print_dbg("Initializing FS...");
  // prepare SPIFFS
  SPIFFS.begin();

  presetsHashMap["preset1"] = preset1;
  presetsHashMap["preset2"] = preset2;
  presetsHashMap["preset3"] = preset3;
  presetsHashMap["preset4"] = preset4;
  presetsHashMap["preset5"] = preset5;
  presetsHashMap["preset6"] = preset6;
  presetsHashMap["preset7"] = preset7;
  presetsHashMap["preset8"] = preset8;
  presetsHashMap["preset9"] = preset9;
  presetsHashMap["preset10"] = preset10;
  presetsHashMap["preset11"] = preset11;
  presetsHashMap["preset12"] = preset12;
  presetsHashMap["presetManual"] = presetManual;

  println_dbg("Ler Config");

  //lerAutorizacao();
  lerUtc();
  setupTime();
  digitalClockDisplay();

  lerTemporizador();
  lerTempAlerta();
  //lerEmail();
  lerRegId();    
  setupWebServer();
  recarregar();
}

void recarregar() {
  zerarTemporizadores();
  tensaoAtual[0] = 0;
  tensaoAtual[1] = 0;
  tensaoAtual[2] = 0;
  gradualCalculado = false;
  if (!modoSetup) {
    printf_dbg("temporizador ativo? %d\n", temporizador);
    if (temporizador) {
      lerAgendador();
    } else {
      agendador = {0, 0, "presetManual", 0};
      lerPreset(agendador.nome);
      aplicarPreset();
    }
    lerStorm();
  } else {
    println_dbg("Entrou no modo setup");
    lerPreset(presetSetup);
    tensaoAtual[0] = presetsHashMap[presetSetup].led1Tensao;
    tensaoAtual[1] = presetsHashMap[presetSetup].led2Tensao;
    tensaoAtual[2] = presetsHashMap[presetSetup].led3Tensao;
     ledcWrite(1,  tensaoAtual[0]);
     ledcWrite(2,  tensaoAtual[1]);
     ledcWrite(3,  tensaoAtual[2]);
//    ledcAnalogWrite(PinBaseTransistor0, tensaoAtual[0]);
//    ledcAnalogWrite(PinBaseTransistor1, tensaoAtual[1]);
//    ledcAnalogWrite(PinBaseTransistor2, tensaoAtual[2]);
    atualizarOled();
  }

}



//le o Autorizacao de dentro do sd e salva na variavel em memoria
void lerAutorizacao() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    autorizacao = root["autorizacao"];
    // Print values.
    println_dbg(autorizacao);
  } else {
    println_dbg("error opening autorizacao.json");
  }
  jsonBuffer.clear();
}

String lerAutorizacaoJson() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(autorizacao);
  } else {
    println_dbg("error opening autorizacao.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o Autorizacao no sd e na variavel em memoria
boolean gravarAutorizacao(String json) {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  autorizacao = root["autorizacao"];
  println_dbg(autorizacao);
  boolean resposta = writeStringToFile("/autorizacao.json", json);
  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
  }


//remove o Autorizacao no sd e na variavel em memoria
boolean removerAutorizacao() {
  autorizacao = "";
  boolean resposta = removeFile("/autorizacao.json");
  println_dbg("Remove done.");
  return resposta;
}


//le o utc de dentro do sd e salva na variavel em memoria
void lerUtc() {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/utc.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    utc = root["utc"];
    // Print values.
    println_dbg(utc);
  } else {
    println_dbg("error opening utc.json");
  }
  jsonBuffer.clear();
}

String lerUtcJson() {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/utc.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(utc);
  } else {
    println_dbg("error opening utc.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o utc no sd e na variavel em memoria
boolean gravarUtc(String json) {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  utc = root["utc"];
  println_dbg(utc);
  boolean resposta = writeStringToFile("/utc.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

//le o email de dentro do sd e salva na variavel em memoria
/*void lerEmail() {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/email.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    email = root["email"].asString();
    emailLigado = root["emailLigado"];
    // Print values.
    println_dbg(email);
  } else {
    println_dbg("error opening email.json");
  }
  jsonBuffer.clear();
}

String lerEmailJson() {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/email.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(email);
  } else {
    println_dbg("error opening email.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o email no sd e na variavel em memoria
boolean gravarEmail(String json) {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  regId = root["email"].asString();
  emailLigado = root["emailLigado"];
  println_dbg(regId);
  boolean resposta = writeStringToFile("/email.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}*/

//le o RegId de dentro do sd e salva na variavel em memoria
void lerRegId() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    regId = root["regId"].asString();
    regIdLigado = root["regIdLigado"];
    // Print values.
    println_dbg(regId);
  } else {
    println_dbg("error opening regId.json");
  }
  jsonBuffer.clear();
}

String lerRegIdJson() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(regId);
  } else {
    println_dbg("error opening regId.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o RegId no sd e na variavel em memoria
boolean gravarRegId(String json) {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  regId = root["regId"].asString();
  regIdLigado = root["regIdLigado"];
  println_dbg(utc);
  boolean resposta = writeStringToFile("/regId.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

void lerTempAlerta() {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/tempAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    
      tempMin = root["tempMin"];  //Implicit cast
      tempMax = root["tempMax"];  //Implicit cast
      println_dbg(tempMin);
      println_dbg(tempMax);
     
  } else {
    println_dbg("error opening tempAlerta.json");
  }
  jsonBuffer.clear();
}


String lerTempAlertaJson() {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/tempAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
  } else {
    println_dbg("error opening tempAlerta.json");
  }
  jsonBuffer.clear();
  return json;
}


boolean gravarTempAlerta(String json) {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
 
    tempMin = root["tempMin"];  //Implicit cast
    tempMax = root["tempMax"];  //Implicit cast
    println_dbg(tempMin);
    println_dbg(tempMax);
  
  println_dbg(json);
  boolean resposta = writeStringToFile("/tempAlerta.json", json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

//le a variavel temporizador e salva na variavel em memoria
void lerTemporizador() {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/timer.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    temporizador = root["timer"];
    // Print values.
    println_dbg(temporizador);
  } else {
    println_dbg("error opening timer.json");
  }
  jsonBuffer.clear();
}

String lerTemporizadorJson() {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/timer.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    // Print values.
    println_dbg(temporizador);
  } else {
    println_dbg("error opening timer.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o temporizador no sd e na variavel em memoria
boolean gravarTemporizador(String json) {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  temporizador = root["timer"];
  println_dbg(temporizador);

  boolean resposta = writeStringToFile("/timer.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
}

//le a variavel storm e salva na variavel em memoria
void lerStorm() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/storm.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    hasStorm = root["storm"];
    setStormMode(root["mode"]);
    // Print values.
    println_dbg(hasStorm);
  } else {
    println_dbg("error opening storm.json");
  }
  jsonBuffer.clear();
}

String lerStormJson() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/storm.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    // Print values.
    println_dbg(hasStorm);
  } else {
    println_dbg("error opening storm.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o storm no sd e na variavel em memoria
boolean gravarStorm(String json) {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  hasStorm = root["storm"];
  setStormMode(root["mode"]);
  
  hadStorm = !hasStorm;
  println_dbg(hasStorm);

  boolean resposta = writeStringToFile("/storm.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
}

//le o preset
void lerPreset(String preset) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/" + preset + ".json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    presetsHashMap[preset].led1Tensao = root["led1Tensao"];
    presetsHashMap[preset].led2Tensao = root["led2Tensao"];
    presetsHashMap[preset].led3Tensao = root["led3Tensao"];

    println_dbg(preset);
    println_dbg(json);
  } else {
    println_dbg("error opening " + preset + ".json");
  }
  jsonBuffer.clear();
}

//le o preset
String lerPresetJson(String preset) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/" + preset + ".json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(preset);
    println_dbg(json);
  } else {
    println_dbg("error opening " + preset + ".json");
  }
  jsonBuffer.clear();
  return json;
}

//salvar preset
boolean gravarPreset(String json) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  println_dbg(json);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  String preset = root["nome"];

  presetsHashMap[preset].led1Tensao = root["led1Tensao"];
  presetsHashMap[preset].led2Tensao = root["led2Tensao"];
  presetsHashMap[preset].led3Tensao = root["led3Tensao"];

  //copiar metodo de cima retorno

  boolean resposta = writeStringToFile("/" + preset + ".json", json);

  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}


//le o agendador e verifica qual o preset deve ser chamado e aplica ele (o metodo de aplicar ainda n foi terminado)
void lerAgendador() {

  digitalClockDisplay();
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  temporizadorPreset = Scheduler();
  String json;
  yield();
  if (getStringFromFile("/sched.json", json)) {
    JsonArray& root = jsonBuffer.parseArray(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    presetsSize=root.size();
    println_dbg("Tamanho vetor :");
    println_dbg(presetsSize);
    
    for (JsonObject& item : root) {
      uint32_t hrInicio = item["hrInicio"];  //Implicit cast
      uint32_t hrFinal = item["hrFinal"];  //Implicit cast
      String nome = item["nome"];  //Implicit cast
      boolean gradual = item["gradual"];  //Implicit cast
      uint32_t duracao = calcularDuracaoPreset(hrInicio, hrFinal);
      println_dbg(hrInicio);
      println_dbg(hrFinal);
      println_dbg(gradual);
      println_dbg(nome);
      println_dbg(duracao);

      println_dbg("teste de hora");
      setarHora(hrInicio, hrFinal);
      println_dbg();
      //inicio.printTo(Serial);
      int hours2 = setHoras(duracao);
      int minutes2 = setMinutos(duracao);
      int seconds2   = setSegundos(duracao);
      
      if (inicio <= Chronos::DateTime::now() && inicio + Chronos::Span::Hours(hours2) + Chronos::Span::Minutes(minutes2) + Chronos::Span::Seconds(seconds2) > Chronos::DateTime::now()) {
        agendador = {hrInicio, hrFinal, nome, gradual};
        print_dbg("Ativar o preset " + nome);
        printf_dbg(" gradual ativo? %d\n", agendador.gradual);
        lerPreset(agendador.nome);
        if (agendador.gradual) {
          gradualCalculado = false;
          aplicarPresetGradual();
        } else {
          duracaoTotal = calcularDuracaoPreset(agendador.hrInicio, agendador.hrFinal) / minuto;
          aplicarPreset();
        }
        print_dbg("AgendadorPreset para : ");
        println_dbg(calcularDuracaoPreset(horaEmMilissegundo(), hrFinal));
        temporizadorAgendador.schedule(lerAgendador, (calcularDuracaoPreset(horaEmMilissegundo(), hrFinal)));
        break;
      }
    }
  } else {
    println_dbg("error opening sched.json");
  }
  jsonBuffer.clear();
}


String lerAgendadorJson() {
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/sched.json", json)) {
    JsonArray& root = jsonBuffer.parseArray(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
  } else {
    println_dbg("error opening sched.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o arquivo agendador, falta colocar a logica para salvar nas variaveis, a definir
boolean gravarAgendador(String json) {
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  yield();
  JsonArray& root = jsonBuffer.parseArray(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  for (JsonObject& item : root) {
    String hrInicio = item["hrInicio"];  //Implicit cast
    String hrFinal = item["hrFinal"];  //Implicit cast
    String nome = item["nome"];  //Implicit cast
    boolean gradual = item["gradual"];  //Implicit cast
    println_dbg(hrInicio);
    println_dbg(hrFinal);
    println_dbg(gradual);
    println_dbg(nome);
  }

  println_dbg(json);

  boolean resposta = writeStringToFile("/sched.json", json);

  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}


//aplicar preset
void aplicarPreset() {
  digitalClockDisplay();
  tensaoAtual[0] = presetsHashMap[agendador.nome].led1Tensao;
  tensaoAtual[1] = presetsHashMap[agendador.nome].led2Tensao;
  tensaoAtual[2] = presetsHashMap[agendador.nome].led3Tensao;
  if(!hasStorm){
    ledcWrite(1,  tensaoAtual[0]);
     ledcWrite(2,  tensaoAtual[1]);
     ledcWrite(3,  tensaoAtual[2]);
//  ledcAnalogWrite(PinBaseTransistor0, tensaoAtual[0]);
//  ledcAnalogWrite(PinBaseTransistor1, tensaoAtual[1]);
//  ledcAnalogWrite(PinBaseTransistor2, tensaoAtual[2]);
  }
  if(temporizador){
  println_dbg("Sem Gradual");
  duracaoAtual = calcularDuracaoPreset(horaEmMilissegundo(), agendador.hrFinal) / minuto;
  
  atualizarOled();
  ///////ATUALIZADO NA VERSÃO 18/////////
  
  desenharTemporizadoRelogio(xCtrl - 2, yCtrl - 2);
  ///////ATUALIZADO NA VERSÃO 18/////////
  
  atualizarIconePreset();
  int segundos = 60 - second();
  print_dbg("Sem Gradual para ");
  println_dbg(segundos);
  if (segundos == 0)
    temporizadorPreset.schedule(aplicarPreset, minuto);
  else
    temporizadorPreset.schedule(aplicarPreset, segundos * 1000);
  }else{
  println_dbg("Manual");
  atualizarOled();
  }
  
}

//aplicar preset gradual
void aplicarPresetGradual() {

  digitalClockDisplay();
  println_dbg("Gradual");
  duracaoAtual = calcularDuracaoPreset(horaEmMilissegundo(), agendador.hrFinal) / minuto;
  if (!gradualCalculado) {
    tensaoGradual[0] = presetsHashMap[agendador.nome].led1Tensao;
    tensaoGradual[1] = presetsHashMap[agendador.nome].led2Tensao;
    tensaoGradual[2] = presetsHashMap[agendador.nome].led3Tensao;
    print_dbg("Tensao Atual: ");
    println_dbg(tensaoAtual[0]);
    //pergar o prox preset
    String proxPreset = proximoPresetAgendador(agendador.nome);
    lerPreset(proxPreset);
    print_dbg("Tensao Prox Preset: ");
    println_dbg(presetsHashMap[proxPreset].led1Tensao);
    diferencaTensao[0] = presetsHashMap[proxPreset].led1Tensao;
    diferencaTensao[1] = presetsHashMap[proxPreset].led2Tensao;
    diferencaTensao[2] = presetsHashMap[proxPreset].led3Tensao;
    print_dbg("Diferenca Tesao: ");
    println_dbg(diferencaTensao[0]);
    //verificar a duracao do preset em minutos
    duracaoTotal = calcularDuracaoPreset(agendador.hrInicio, agendador.hrFinal) / minuto;
    print_dbg("Duracao Total: ");
    println_dbg(duracaoTotal);
    gradualCalculado = true;
  } 
  //aplica tensaoGradual no led
  tensaoAtual[0] = map(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[0], diferencaTensao[0]);
  tensaoAtual[1] = map(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[1], diferencaTensao[1]);
  tensaoAtual[2] = map(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[2], diferencaTensao[2]);
if(!hasStorm){
//  ledcAnalogWrite(PinBaseTransistor0, tensaoAtual[0]);
//  ledcAnalogWrite(PinBaseTransistor1, tensaoAtual[1]);
//  ledcAnalogWrite(PinBaseTransistor2, tensaoAtual[2]);
     ledcWrite(1,  tensaoAtual[0]);
     ledcWrite(2,  tensaoAtual[1]);
     ledcWrite(3,  tensaoAtual[2]);
  }
  print_dbg("Duracao Restante: ");
  println_dbg(duracaoAtual);
  print_dbg("Posicao Atual: ");
  println_dbg((duracaoTotal - duracaoAtual));
  print_dbg("Tensao Atual: ");
  println_dbg(tensaoAtual[0]);


  atualizarOled();
  ///////ATUALIZADO NA VERSÃO 18/////////
  desenharTemporizadoRelogio(xCtrl - 2, yCtrl - 2);
  ///////ATUALIZADO NA VERSÃO 18/////////
  atualizarIconePreset();

  int segundos = 60 - second();
  print_dbg("Gradual para ");
  println_dbg(segundos);
  if (segundos == 0)
    temporizadorPreset.schedule(aplicarPresetGradual, minuto);
  else
    temporizadorPreset.schedule(aplicarPresetGradual, segundos * 1000);
}



void zerarTemporizadores() {
  temporizadorAgendador = Scheduler();
  temporizadorPreset = Scheduler();
}

String proximoPresetAgendador(String preset) {
  String retorno = "";
  int id = presetsHashMap.indexOf(preset);
  int idPM = presetsHashMap.indexOf("presetManual");
  println_dbg("ID");
  println_dbg(id);
  println_dbg(idPM);
  println_dbg("SIZE");
  println_dbg(presetsSize);
  if (id == presetsSize - 1) {
    println_dbg("proximo preset nao existe");
    println_dbg(id + 1);
    retorno = presetsHashMap.keyAt(0);
  } else if (id < presetsSize - 1) {
    retorno = presetsHashMap.keyAt(id + 1);
    println_dbg("retorno");
    println_dbg(retorno);
  }
  return retorno;

}


