/* Version */
#define AQUA_SUN      "v0.9"

/*
  DEFINE LEDS
*/
#define PinBaseTransistor0        A14
#define PinBaseTransistor1        A15
#define PinBaseTransistor2        A16
/*
  DEFINE TERMOMETRO
*/
#define ONE_WIRE_BUS 22
/*3
  DEFINE RESET PIN
*/
#define BUTTONPIN 23
#define OTAPIN 21

/*
  DEFINE OLED:
  SCK = GPIO04 -> D1
  SDA = GPIO05 -> D2
*/
#define OLED_SCK  22
#define OLED_SDA  21

/*Json buffer sizes*/
#define AGENDADOR_JSON_SIZE (JSON_ARRAY_SIZE(12) + 12*JSON_OBJECT_SIZE(4))
#define PRESET_JSON_SIZE (JSON_OBJECT_SIZE(4))
#define UTC_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define EMAIL_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define REGID_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define AUTORIZACAO_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define TEMPORIZADOR_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define STORM_JSON_SIZE (JSON_OBJECT_SIZE(7))
#define TEMPALERTA_JSON_SIZE (JSON_OBJECT_SIZE(2))

/* Hostname*/
#define HOSTNAME_DEFAULT        "AQUASUNDEVICE-32"

/* for Debug */
/* for Debug */
#define SERIAL_DEBUG            true

#if SERIAL_DEBUG == true
#define DEBUG_SERIAL_STREAM     Serial
#define print_dbg               DEBUG_SERIAL_STREAM.print
#define printf_dbg              DEBUG_SERIAL_STREAM.printf
#define println_dbg             DEBUG_SERIAL_STREAM.println
#else
#define DEBUG_SERIAL_STREAM     NULL
#define print_dbg               // No Operation
#define printf_dbg              // No Operation
#define println_dbg             // No Operation
#endif


