///////////////////////Desenhos/////////////////////////
void desenharImagemWifi() {
  // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
  // on how to create xbm files
  display.drawXbm(34, 22, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

void desenharAtencao(int x, int y) {
  display.drawXbm(x, y, atencao_peq_width, atencao_peq_height, atencao_peq_bits);
  display.display();
}

void desenharBarraAlerta() {
  display.drawXbm(5, 30, barraAlerta_width, barraAlerta_height, barraAlerta_bits);
  display.display();
}

void desenharImagemRedes() {
  display.drawXbm(44, 14, redes_width, redes_height, redes_bits);
  display.display();
}

void desenharImagemSinalWlan() {
  display.drawXbm(44, 20, sinalWlan_width, sinalWlan_height, sinalWlan_bits);
  display.display();
}

void desenharCadeado() {
  display.drawXbm(50, 30, cadeado_width, cadeado_height, cadeado_bits);
  display.display();
}

void limparAreaStringTemperatura(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  //display.fillRect(55, 47, 73, 17);
  display.display();
  display.setColor(WHITE);
}

void desenharTermometro() {
  display.drawXbm(110, 5, termometro_width, termometro_height, termometro_bits);
  display.display();
}

void limparAreaNivelTermometro(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  display.display();
  display.setColor(WHITE);
}

void desenharNivelTermometro(float nivel){
  float limite = 50;
  float valor = (nivel / limite) * 30;
  if (nivel > 0) {
    display.fillRect(116, 35 - (int) valor, (termometro_width - 12), (int) valor + 3);
  }
}

void limparAreaGraficoLeds(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  display.display();
  display.setColor(WHITE);
}

void desenharStorm() {
  if(hasTemp){
    display.drawXbm(xStorm, yStorm, storm_MIN_width, storm_MIN_height, storm_MIN_bits);
  }else{
    display.drawXbm(xStorm, yStorm, storm_MAX_width, storm_MAX_height, storm_MAX_bits);
  }   
  display.display();  
}

void desenharGraficoLeds(int x, int y, float tamx, int tamy, String idLed) {
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(x , 0, idLed);
  display.drawRect(x, 13, tamx, 50);
  y = 63 - tamy;
  display.fillRect(x, y, tamx, tamy);
  display.display();
}

void limparAreaControles(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  //display.fillRect(52, 13, 25, 25);
  display.display();
  display.setColor(WHITE);
}

void desenharControleManual(int x, int y) {
  display.drawXbm(x, y, manual_width, manual_height, manual_bits);
  display.display();
}

void desenharControleTemporizado(int x, int y) {
  display.drawXbm(x, y, temporizado_width, temporizado_height, temporizado_bits);
  display.display();
}

void desenharTemporizadoRelogio(int x, int y) {
  int clockRadius = 10.9;
  int limitador = map(duracaoTotal - duracaoAtual, 0, duracaoTotal, 0, 360);
  //int limitador = (360 / duracaoTotal) * (duracaoTotal - duracaoAtual);
  print_dbg("Relogio :");
  println_dbg(limitador);
  for ( int z = 0; z <= limitador; z ++ ) {
    float angle = z ;
    angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
    int x2 = ( x + ( sin(angle) * clockRadius ) );
    int y2 = ( y - ( cos(angle) * clockRadius ) );
    int x3 = ( x + ( sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
    int y3 = ( y - ( cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
    ///////ATUALIZADO NA VERSÃO 18/////////
    display.drawLine( x2 + 14 , y2 + 14 , x3 + 14 , y3 + 14);
    display.drawLine( x2 + 15 , y2 + 15 , x3 + 15 , y3 + 15);
    display.drawLine( x2 + 16 , y2 + 16 , x3 + 16 , y3 + 16);
  }
  display.display();
}

void desenharModoConfig(int x, int y) {
  display.drawXbm(x, y, config_width, config_height, config_bits);
  display.display();
}

void limparAreaSinalWIFI(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  //display.fillRect(83, 14, 25, 20);
  display.display();
  display.setColor(WHITE);
}

void desenharSinalWIFI(int x, int y) {
  display.drawXbm(x, y, sinal_WIFI_width, sinal_WIFI_height, sinal_WIFI_bits);
  display.display();
}

void desenharNOSinalWIFI(int x, int y) {
  display.drawXbm(x, y, no_sinal_WIFI_width, no_sinal_WIFI_height, no_sinal_WIFI_bits);
  display.display();
}

void desenharNOInternet(int x, int y) {
  display.drawXbm(x, y, no_Internet_width, no_Internet_height, no_Internet_bits);
  display.display();
}

void desenharfundoPreset(int x, int y) {
  display.drawXbm(x, y, fundo_preset_width, fundo_preset_height, fundo_preset_bits);
  display.display();
}

void limparAreaFundoPreset(int x, int y, int r) {
  display.setColor(BLACK);
  display.fillCircle(x, y, r);
  //display.fillRect(x, y, tamx, tamy);
  display.display();
  display.setColor(WHITE);
}

void limparAreaFundoPresetIMG(int x, int y) {
  display.setColor(BLACK);
  display.drawXbm(x, y, fundo_preset_IMG_width, fundo_preset_IMG_height, fundo_preset_IMG_bits);
  display.display();
  display.setColor(WHITE);
}

void desenharFundoPreset(int x, int y, String preset) {
  String strPreset =  String(presetsHashMap.indexOf(preset) + 1);
  display.setFont(ArialMT_Plain_10);
  display.drawString(x , y, strPreset);
  display.display();
}

void desenharBateria(int x, int y, int carga) {
  display.drawRect(x, y, 10, 3);
  display.drawVerticalLine((x + 11), (y + 1), 2);
  display.fillRect((x + 1), (y + 1), carga, 2);
}

void desenharSemBateria(int x, int y) {
  display.drawXbm(x, y, semBateria_width, semBateria_height, semBateria_bits);
  display.display();
}
/////////////////////FIM Desenhos///////////////////////


