void setupOled(){
  println_dbg("Testando OLED! (SSD1306 LIB)");
  
  display.init();

  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.drawString(0 , 0, "Testando OLED!");
  display.display();
  delay(3 * 1000);
  display.clear();
}


void setupCoordenadasOled(bool setupTermometro){
if (setupTermometro) {
  ///////STORM//////////////
    xStorm = 1;
    yStorm = 12;
    hasTemp=true;   
    ///////GRAFICO LEDS//////////////
    xLed = 5;
    yLed = 13;
    tamXLed = 10;
    tamYLed = 0;
    tamXLimparAreaLed = 47;
    tamYLimparAreaLed = 64;
    espacoXLed = 15;
    ////////CONTROLES///////////////
    xCtrl = 52;
    yCtrl = 13;
    ////////SINAL WIFI/////////////
    xWifi = 83;
    yWifi = 15;
    ////////PRESET////////////////
    xPrst = 62;
    yPrst = 19;
    rPrst = 6;
    tamXLimparAreaPreset = 65;
    tamYLimparAreaPreset = 25;
    ///////ATUALIZADO NA VERSÃO 18//////
    tamXLimparAreaPresetIMG = 57;
    tamYLimparAreaPresetIMG = 18;
  } else {
     ///////STORM//////////////
    xStorm = 18;
    yStorm = 1;  
    hasTemp=false; 
    ///////GRAFICO LEDS//////////////
    xLed = 5;
    yLed = 13;
    tamXLed = 15;
    tamYLed = 0;
    tamXLimparAreaLed = 80;
    tamYLimparAreaLed = 64;
    espacoXLed = 30;
    ////////CONTROLES///////////////
    xCtrl = 96;
    yCtrl = 38;
    ////////SINAL WIFI/////////////
    xWifi = 96;
    yWifi = 10;
    ////////PRESET////////////////
    xPrst = 106;
    yPrst = 44;
    rPrst = 6;
    tamXLimparAreaPreset = 109;
    tamYLimparAreaPreset = 50;
    ///////ATUALIZADO NA VERSÃO 18//////
    tamXLimparAreaPresetIMG = 101;
    tamYLimparAreaPresetIMG = 43;
  }
}
