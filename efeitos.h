


void storm1(){  

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      
      delay(20 + random(DURATION));
      ledcWrite(1,  0);
     ledcWrite(2,  10);    

      delay(10);
    }
}
void storm2(){

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(20 + random(DURATION));
      ledcWrite(2, 10);
      ledcWrite(1, 0);
      delay(10);
    }
}
void storm3(){
   
    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(2, 10);
      delay(10);
    }
}
void storm4(){

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(1, 0);
      delay(10);
    }
}

void storm5(){

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(20 + random(DURATION));
      ledcWrite(2, 10);
      ledcWrite(1, 0);
      delay(10);
    }
    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(1, 0);
      delay(10);
    }
}
void storm6(){   

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      delay(20 + random(DURATION));
      ledcWrite(1, 0);
      ledcWrite(2, 10);
      delay(10);
    }
    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(2, 10);
      delay(10);
    }
}

void storm7(){

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(1, 0);
      delay(10);
    }

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      delay(20 + random(DURATION));
      ledcWrite(2, 10);
      ledcWrite(1, 0);
      delay(10);
    } 
}

void storm8(){  

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      delay(10 + random(DURATION));
      ledcWrite(2, 10);
      delay(10);
    }

    for (int i=0; i< random(TIMES_MIN,TIMES_MAX); i++)
    {
      
      ledcWrite(1, random(VOLT_MIN,VOLT_MAX));
      ledcWrite(2, random(VOLT_MIN,VOLT_MAX));
      delay(20 + random(DURATION));
      ledcWrite(1, 0);
      ledcWrite(2, 10);
      delay(10);
    }
}


void fadeIn(){
  println_dbg("FadeIn");
    for (int x=1; x <= 100; x++){
       ledcWrite(1, map(x, 0, 100, tensaoAtual[0], 0));
       ledcWrite(2, map(x, 0, 100, tensaoAtual[1], 10));
       ledcWrite(3, map(x, 0, 100, tensaoAtual[2], 0));
       delay(30);       
    }
}

void fadeOut(){
  println_dbg("FadeOut");
    for (int x=1; x <= 100; x++){
       ledcWrite(1, map(x, 0, 100, 0 , tensaoAtual[0]));
       ledcWrite(2, map(x, 0, 100, 10, tensaoAtual[1]));
       ledcWrite(3, map(x, 0, 100, 0, tensaoAtual[2]));
       delay(30);       
    }
}

void doStorm(){
  println_dbg("Strom Started");
  int opt = random(9);
  lastTime += waitTime;
   waitTime = random(BETWEEN_MIN,BETWEEN_MAX);
  
  switch ( opt )
  {
     case 1 :
      println_dbg("case 1");
      storm1();
     break;
 
     case 2 :
       println_dbg("case 2");
       storm2();         
     break;
     
     case 3 :
       println_dbg("case 3");
       storm3();
     break;
     
     case 4 :
       println_dbg("case 4");
       storm4();
     break;
     
     case 5 :
       println_dbg("case 5");
       if(stormMode==0){
        storm1();
       }else{
        storm5();
       }       
     break;
     
     case 6 :
       println_dbg("case 6");
       if(stormMode==0){
        storm2();
       }else{
        storm6();;
       }       
     break;
     
     case 7 :
       println_dbg("case 7");
       if(stormMode==0){
        storm3();
       }else{
        storm7();;
       }
     break;
     
     case 8 :
       println_dbg("case 8");
       if(stormMode==0){
        storm4();
       }else{
        storm8();;
       }
     break;
     
     default :
       println_dbg("default");
       
  }
}

void setStormMode(int opt){
  println_dbg("Storm Mode");
  
  switch ( opt )
  {
     case 0 :
      println_dbg("Calm");
      stormMode=0;
      BETWEEN_MIN =6000;
      BETWEEN_MAX =8000;
      VOLT_MIN =100;
      VOLT_MAX =200;
      DURATION =20;
      TIMES_MIN =2;
      TIMES_MAX =4;
     break;
     
     case 1 :
       println_dbg("Normal");
       stormMode=1;
       BETWEEN_MIN =1000;
       BETWEEN_MAX =3000;
       VOLT_MIN =100;
       VOLT_MAX =256;
       DURATION =40;
       TIMES_MIN =4;
       TIMES_MAX =6;
     break;
     
     case 2 :
       println_dbg("Intenso");
       stormMode=2;
       BETWEEN_MIN =1000;
       BETWEEN_MAX =2000;
       VOLT_MIN =150;
       VOLT_MAX =256;
       DURATION =40;
       TIMES_MIN =5;
       TIMES_MAX =7;
     break;
             
     default :
       println_dbg("default");
       
  }
}

