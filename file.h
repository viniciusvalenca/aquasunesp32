
#ifndef __FILE_H__
#define __FILE_H__

#include <WiFi.h>

bool writeStringToFile(String path, String& dataString);
bool getStringFromFile(String path, String& dataString);
bool removeFile(String path);

#endif

