////////////////////////Mensagens////////////////////////
void mensagemIniciarWLAN(){
  println_dbg("\nVer 0.9");
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(1, 1, "Ver 1.9");
  delay(1000);
  display.clear();
  println_dbg();
  print_dbg("Conectando a: ");
  println_dbg(WiFi.SSID());
  display.drawString(0 , 0, "Conectando a rede WLAN... ");
  desenharImagemWifi();
  println_dbg();
  display.display();
  delay(4000);
  sprintf(endIP, "%3d.%3d.%3d.%3d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  print_dbg("CHAR IP: ");
  println_dbg(endIP);
}

void mensagemLocalizandoDispositivos()
{
  display.clear();  
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 20, "LOCALIZANDO");
  display.drawString(64 , 40 , "DISPOSITIVOS...");
  //desenharImagemLogo();
  display.display();
}

void mensagemNenhumaRedeEncontrada()
{
  display.clear();  
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 10, "NENHUMA");
  display.drawString(64 , 23 , "REDE");
   display.drawString(64 , 36 , "ENCONTRADA!");
  //desenharImagemLogo();
  display.display();
}

void mensagemModoReconfiguracao()
{
  display.clear();  
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 10, "MODO DE");
  display.drawString(64 , 23 , "RECONFIGURACAO");
   display.drawString(64 , 36 , "WIFI");
   display.drawString(64 , 49 , "ATIVADO!");
  //desenharImagemLogo();
  display.display();
}

void mensageLocalizando() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0 , 0, "Localizando...");
  display.display();
}

void mensagemEnderecoIP() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0 , 0, "Endereco IP:");
  display.drawString(0 , 10, endIP);
  display.display();
}

void mensagemWifiConectado() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "WiFi conectado");
  display.display();
  println_dbg("");
  println_dbg("WiFi conectado");
}

void mensagemTentandoConectar() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Tentando Conectar");
  desenharImagemWifi();
  display.display();
}

void mensageminicial()
{
  display.clear();
  display.drawHorizontalLine(0, 30, 128);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 33, "SOLICITAR");
  display.drawString(64 , 46 , "ATENDIMENTO");
  display.drawHorizontalLine(0, 63, 128);
  //desenharImagemLogo();
  display.display();
}

void mensagemip()
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Aguarde");
  display.drawString(64 , 10, "Iniciando rede");
  display.display();
}

void mensagemERRORede() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Falha ao");
  display.drawString(64 , 10, "iniciar a Rede");
  delay(1000);
  display.drawString(64 , 20, "Por favor");
  display.drawString(64 , 30, "Entre em contato");
  display.drawString(64 , 40, "com o suporte!");
  display.display();
}

void mensagemRespostaServidor(String resp, char respChar[]) {
  display.clear();
  display.drawHorizontalLine(0, 0, 128);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 2, "Por favor Aguarde.");
  display.drawString(64 , 12, respChar);
  display.drawHorizontalLine(0, 26, 128);
  display.display();
  println_dbg("Resposta do servidor: " + resp);
}

void mensagemServidorRespondendo() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Servidor");
  display.drawString(64 , 10, "Respondendo");
  display.display();
}

void mensagemFalhaConexao() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Falha na conexao");
  display.drawString(64 , 10, "Reconectando...");
  display.display();
}

void mensagemConsultandoServidor() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_10);
  display.drawString(64 , 0, "Consultando");
  display.drawString(64 , 10, "Servidor...");
  display.display();
}

void mensagemProcurandoRedes() {
  println_dbg("");
  println_dbg("Procurando redes ... ");
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.clear();
  display.drawString(0 , 0, "Procurando redes ...");
  display.display();
}

void mensagemRedesEncontradas() {
  println_dbg("Redes encontradas");
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.clear();
  display.drawString(0 , 0, "Redes encontradas\n");
  display.display();
}
//////////////////////FIM Mensagens//////////////////////
