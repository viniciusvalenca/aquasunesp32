
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) print_dbg("0");
    print_dbg(deviceAddress[i], HEX);
  }
}

bool setupTermometro() {
  bool resposta = false;
  println_dbg("Configurando o Termômetro (biblioteca Dallas Temperature)");
  // localiza o termômetro no barramento.
  print_dbg("Localizando termômetros...");
  sensors.begin();
  print_dbg("Encontrado(s): ");
  print_dbg(sensors.getDeviceCount(), DEC);
  println_dbg(" termômetro(s)");

  // report parasite power requirements
  print_dbg("Parasite power is: ");
  if (sensors.isParasitePowerMode()) println_dbg("ON");
  else println_dbg("OFF");
  mensagemLocalizandoDispositivos();

  if (!sensors.getAddress(insideThermometer, 0)) {
    println_dbg("Não foi possível achar o endereço para o termômetro 0");
  } else {
    resposta = true;
  }
  print_dbg("Temômetro 0, #Endereço: ");
  printAddress(insideThermometer);
  println_dbg();

  // set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
  sensors.setResolution(insideThermometer, 9);

  print_dbg("Device 0 Resolution: ");
  print_dbg(sensors.getResolution(insideThermometer), DEC);
  println_dbg();
  return resposta;
}

void displayTemperatura(float temperatura) {
  limparAreaStringTemperatura(55, 47, 73, 17);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  //display.drawString(0 , 0, "Temperatura:");
  //display.setFont(ArialMT_Plain_10);
  display.drawString(60 , 47, (String)temperatura);
  display.drawCircle(105, 50, 2);//símbolo grau
  display.drawString(110 , 47, "C");
  limparAreaNivelTermometro(116, 6, (termometro_width - 12), 26);
  desenharNivelTermometro(temperatura);
  //desenharNivelTermometro(62);
  display.display();
}

void verificarTemperatura(DeviceAddress deviceAddress)
{
  tempC = sensors.getTempC(deviceAddress);
  //print_dbg("Temp C: ");
  //println_dbg(tempC);

  //print_dbg(" Temp F: ");
  //println_dbg(DallasTemperature::toFahrenheit(tempC)); // Converts tempC to Fahrenheit
}

void imprimirTemperatura(DeviceAddress deviceAddress)
{
  if (tempC != temperaturaAtual) {
    displayTemperatura(tempC);
    temperaturaAtual = tempC;
  }
}
