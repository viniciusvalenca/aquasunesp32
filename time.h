

void sincronizarRTC() {
  if (RTC.set(now()) == 0)
    println_dbg("RTC Sincronizado");
  else
    println_dbg("Erro ao Sincronizar o RTC ");
}


void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  print_dbg(":");
  if (digits < 10)
    print_dbg('0');
  print_dbg(digits);
}


void digitalClockDisplay()
{
  // digital clock display of the time
  print_dbg(hour());
  printDigits(minute());
  printDigits(second());
  print_dbg(" ");
  print_dbg(day());
  print_dbg(".");
  print_dbg(month());
  print_dbg(".");
  print_dbg(year());
  println_dbg();
}

boolean verificarInternet() {
  print_dbg("Teste de internet:");
  println_dbg(adr);
  boolean resposta = ping_start(adr, 4, 0, 0, 5);
  if (resposta)
    println_dbg("Com Internet!!");
  else
    println_dbg("Sem Internet!!");
  return true;
}



void timeUpdate() {
  if (timeProvider == 0)
    setSyncProvider(getNtpTime);
}


//retorna a hora atual em milisegundos
int horaEmMilissegundo() {
  return (hour() * 60 * 60 * 1000) + (minute() * 60 * 1000) + (second() * 1000);
}

void iniciarNTP() {
  println_dbg("Starting UDP");
  Udp.begin(3333);
  print_dbg("Local port: ");
  println_dbg(Udp.begin(3333));
  println_dbg("waiting for sync");
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
  timeProvider = 0;
  sincronizarRTC();
}

void iniciarRTC() {
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  if (timeStatus() != timeSet)
    println_dbg("Unable to sync with the RTC");
  else
    println_dbg("RTC has set the system time");
  timeProvider = 1;
}

void setupTime() {
  agendadorMudarProvider = 0;
  if (verificarInternet()) {
    estadoWifi=1;
    iniciarNTP();
  } else {
    estadoWifi=2;
    iniciarRTC();
  }
}

int calcularDuracaoPreset(uint32_t inicio, uint32_t fim) {
  uint32_t ms;
  if (inicio <= fim) {
    ms = fim - inicio;
  } else {
    ms = (fim + 86400000) - inicio;
  }
  return ms;
}

int setHoras(uint32_t ms) {
  return (int) ((ms / (1000 * 60 * 60)) % 24);
}
int setMinutos(uint32_t ms) {
  return (int) ( ms / (1000 * 60)) % 60;
}
int setSegundos(uint32_t ms) {
  return (int) ( ms / 1000) % 60 ;
}


void setarHora(uint32_t hrInicio, uint32_t hrFinal) {
  Chronos::DateTime inicioTmp;
  int inicioHoraTmp = setHoras(hrInicio);
  int inicioMinutoTmp = setMinutos(hrInicio);
  int inicioSegundoTmp = setSegundos(hrInicio);
  inicioTmp.setYear(year());
  inicioTmp.setMonth(month());
  inicioTmp.setDay(day());
  inicioTmp.setHour(inicioHoraTmp);
  inicioTmp.setMinute(inicioMinutoTmp);
  inicioTmp.setSecond(inicioSegundoTmp);

  Chronos::DateTime fimTmp;
  int fimHoraTmp = setHoras(hrFinal);
  int fimMinutoTmp = setMinutos(hrFinal);
  int fimSegundoTmp = setSegundos(hrFinal);
  fimTmp.setYear(year());
  fimTmp.setMonth(month());
  fimTmp.setDay(day());
  fimTmp.setHour(fimHoraTmp);
  fimTmp.setMinute(fimMinutoTmp);
  fimTmp.setSecond(fimSegundoTmp);
  if (Chronos::DateTime::now() >= inicioTmp) {
    inicio.setYear(year());
    inicio.setMonth(month());
    inicio.setDay(day());
    inicio.setHour(inicioHoraTmp);
    print_dbg(inicio.hour());
    print_dbg(":");
    inicio.setMinute(inicioMinutoTmp);
    print_dbg(inicio.minute());
    print_dbg(":");
    inicio.setSecond(inicioSegundoTmp);
    println_dbg(inicio.second());
    println_dbg("Antes");
    inicio.printTo(Serial);
  } else {
    uint32_t duracao = calcularDuracaoPreset(hrInicio, hrFinal);
    int hoursDuracaoTmp = setHoras(duracao);
    int minutesDuracaoTmp = setMinutos(duracao);
    int secondsDuracaoTmp   = setSegundos(duracao);
    inicio = fimTmp;
    inicio -= Chronos::Span::Hours(hoursDuracaoTmp);
    inicio -= Chronos::Span::Minutes(minutesDuracaoTmp);
    inicio -= Chronos::Span::Seconds(secondsDuracaoTmp);
    println_dbg("Depois");
    inicio.printTo(Serial);
  }
}

