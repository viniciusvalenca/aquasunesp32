////////VARS PRIMEIRAPASSAGEM/////////
boolean firstRun = true;
///////////////////////////////////

////////VARS ESP8266Wifi.h/////////
char endIP[24];
char ssid[24];
///////////////////////////////////

////////VARS RESETPIN/////////
int buttonState = 0;
///////////////////////////////////

////////VARS OTA/////////
int buttonOTAState = 0;
boolean OTA_ENABLED = false;
///////////////////////////////////

////////VAR WEBSERVER/////////
WebServer server(80);
////////////////////////////////////

////////VAR PCA8596/////////
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(&Wire, 0x40);
////////////////////////////////////

////////VAR FIREBASE/////////
HTTPClient http;
String serve = "AAAAqRuRuTs:APA91bE0uls4mvjAmKUlL_jRpV9J1Kp53sUZBqPxur5muGWSUJcEM1aTX64swyc_niL7jCBXgVLs1R0IlpiC49KutfHHisBaoHcUrwT_MGt96CKnGjrXMshd31j5rY4GobuSgvsXAglh";
////////////////////////////////////

////////VAR OLED/////////
SSD1306  display(0x3c, OLED_SDA, OLED_SCK);
////////////////////////////////////

////////VAR EMAIL/////////
//String  email="";
//boolean emailLigado=false;
////////////////////////////////////

////////VAR EMAIL_SMTP/////////
//String  smtp="";
//String  login="";
//String  password="";
//String  sender="";
//int smtpPort;
//boolean sslLigado=false;
////////////////////////////////////

////////VAR REGID/////////
String  regId = "";
boolean regIdLigado = false;
////////////////////////////////////

//////////VARS TERMOMETRO///////////
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress insideThermometer;
float temperaturaAtual = 0;
float tempC = 0;
float tempMin = 0;
float tempMax = 0;
boolean alertaTemp = false;
////////////////////////////////////

////////VARS Float To String/////////
char charBuffer[25];
////////////////////////////////////

////////VAR Autorizacao/////////
const char* autorizacao;
////////////////////////////////////

////////VAR Modo Setup/////////
boolean modoSetup = false;
String presetSetup = "";
////////////////////////////////////

////////VARS Agendador/////////
Scheduler temporizadorAgendador = Scheduler();
Scheduler temporizadorPreset = Scheduler();
boolean temporizador = false;

struct Agendador {
  uint32_t hrInicio ;
  uint32_t hrFinal;
  String nome ;
  boolean gradual ;
};

Agendador agendador ;
Chronos::DateTime inicio;
////////////////////////////////////

////////VARS PING/////////
IPAddress adr = IPAddress(8, 8, 8, 8);
////////////////////////////////////

////////VARS TIMEPROVIDER/////////
int timeProvider; //0 = NTP   1 = RTC
int agendadorMudarProvider = 0; //0 = inativo   1 = ativo
Scheduler mudarProvider = Scheduler();
////////////////////////////////////

////////VARS NTP/////////
WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
// NTP Servers:
static const char ntpServerName[] = "pool.ntp.org";
int16_t utc = -3; //UTC -3:00 Brazil
////////////////////////////////////

////////VARS Preset/////////
struct Preset {
  String nome;
  long led1Tensao;
  long led2Tensao;
  long led3Tensao;
};

Preset preset1 = {"preset1", 0, 0, 0};
Preset preset2 = {"preset2", 0, 0, 0};
Preset preset3 = {"preset3", 0, 0, 0};
Preset preset4 = {"preset4", 0, 0, 0};
Preset preset5 = {"preset5", 0, 0, 0};
Preset preset6 = {"preset6", 0, 0, 0};
Preset preset7 = {"preset7", 0, 0, 0};
Preset preset8 = {"preset8", 0, 0, 0};
Preset preset9 = {"preset9", 0, 0, 0};
Preset preset10 = {"preset10", 0, 0, 0};
Preset preset11 = {"preset11", 0, 0, 0};
Preset preset12 = {"preset12", 0, 0, 0};
Preset presetManual = {"presetManual", 0, 0, 0};
CreateHashMap(presetsHashMap, String, Preset, 13);
int presetsSize = 0;
int duracaoTotal = 0;
int duracaoAtual = 0;
int minuto = 60000;
////////////////////////////////////

////////VARS MetodoGradual/////////
boolean gradualCalculado = false;

float tensaoAtual[] = {0, 0, 0};
float diferencaTensao[] = {0, 0, 0};
float tensaoGradual[] = {0, 0, 0};
////////////////////////////////////

////////VARS Storm////////////
boolean hasStorm = false;
boolean hadStorm = false;
boolean hadFadeIn = false;
boolean hadFadeOut = false;

int BETWEEN_MIN = 3000;
int BETWEEN_MAX = 6000;
int VOLT_MIN = 100;
int VOLT_MAX = 255;
int DURATION = 43;
int TIMES_MIN = 2;
int TIMES_MAX = 7;
int stormMode;
unsigned long lastTime = 0;
int waitTime = 0;
////////////////////////////////////

////////VARS Grafico/////////
int tensaoA = 0;
int tensaoB = 0;
int tensaoC = 0;
/////////////////////////////

////////VARS ESTADO WIFI/////
int estadoWifi = 0;
int estadoWifiAtual = 0;
/////////////////////////////

///////COORDENADAS DESENHOS///////
boolean hasTemp;
int xStorm;
int yStorm;
///////GRAFICO LEDS//////////////
int xLed;
int yLed;
int tamXLed;
int tamYLed;
int tamXLimparAreaLed;
int tamYLimparAreaLed;
int espacoXLed;
////////CONTROLES///////////////
int xCtrl;
int yCtrl;
////////SINAL WIFI/////////////
int xWifi;
int yWifi;
////////PRESET////////////////
int xPrst;
int yPrst;
int rPrst;
int tamXLimparAreaPreset;
int tamYLimparAreaPreset;
int tamXLimparAreaPresetIMG;
int tamYLimparAreaPresetIMG;
//////////////////////////////
