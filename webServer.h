
void setAutorizacaoController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarAutorizacao(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  //recarregar();
}

void getAutorizacaoController() {
  String json = lerAutorizacaoJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}


void setTempAlertaController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarTempAlerta(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  //recarregar();
}

void getTempAlertaController() {
  String json = lerTempAlertaJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setPresetController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarPreset(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  if (!modoSetup) {
    recarregar();
  }
}

void getPresetController() {
  if (server.hasArg("preset") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String preset = server.arg("preset");
  String json = lerPresetJson(preset);
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setUtcController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarUtc(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  timeUpdate();
  recarregar();
}

void getUtcController() {
  String json = lerUtcJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setAgendadorController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarAgendador(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void getAgendadorController() {
  String json = lerAgendadorJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}


/*void setEmailController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarEmail(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
}

void getEmailController() {
  String json = lerEmailJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}*/

void setRegIdController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarRegId(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
}

void getRegIdController() {
  String json = lerRegIdJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setTimerController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarTemporizador(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void getTimerController() {
  String json = lerTemporizadorJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setStormController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarStorm(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  //recarregar();
}

void getStormController() {
  String json = lerStormJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void getTemperaturaController() {
  String message = "";
  dtostrf(tempC, 4, 2, charBuffer);
  for (int i = 0; i < sizeof(charBuffer); i++)
  {
    message += (String)charBuffer[i];
  }
  message.replace(" ", "");
  server.send(200, "text / plain", (String)message);//Returns the HTTP response
}

void configuracaoController() {
  if (server.hasArg("setup") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  modoSetup = server.arg("setup").toInt();


  String message = "";
  if ( modoSetup ) {
    message = "true";
    presetSetup = server.arg("preset");
  } else {
    message = "false";
  }

  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void resetController() {
  server.send(200, "text/plain", "true");
  println_dbg("true");
  resetDefaults();
}

void ledsController() {
  tensaoAtual[0] = server.arg("led1Tensao").toInt();
  tensaoAtual[1] = server.arg("led2Tensao").toInt();
  tensaoAtual[2] = server.arg("led3Tensao").toInt();
  ledcWrite(1, tensaoAtual[0]);
  ledcWrite(2, tensaoAtual[1]);
  ledcWrite(3, tensaoAtual[2]);

  tensaoA = (tensaoAtual[0] / 1024) * 50;
  tensaoB = (tensaoAtual[1] / 1024) * 50;
  tensaoC = (tensaoAtual[2] / 1024) * 50;
  
  limparAreaGraficoLeds(xLed, tamYLed, tamXLimparAreaLed, tamYLimparAreaLed);//ATUALIZADO
  desenharGraficoLeds(xLed, yLed, tamXLed, tensaoA, "A");//ATUALIZADO
  desenharGraficoLeds(xLed + espacoXLed, yLed, tamXLed, tensaoB, "B");//ATUALIZADO
  desenharGraficoLeds(xLed + (espacoXLed * 2), yLed, tamXLed, tensaoC, "C"); //ATUALIZADO


  server.send(200, "text / plain", "true");        //Returns the HTTP response
}

void setupWebServer() {
  println_dbg("Configurando WebServer!");

  //Iniciar o servidor
  server.on("/setAutorizacao", HTTP_POST, setAutorizacaoController);
  server.on("/getAutorizacao", HTTP_GET, getAutorizacaoController);
  server.on("/setTempAlerta", HTTP_POST, setTempAlertaController);
  server.on("/getTempAlerta", HTTP_GET, getTempAlertaController);
  server.on("/setPreset", HTTP_POST, setPresetController);
  server.on("/getPreset", HTTP_GET, getPresetController);
  server.on("/setAgendador", HTTP_POST, setAgendadorController);
  server.on("/getAgendador", HTTP_GET, getAgendadorController);
  //server.on("/setEmail", HTTP_POST, setEmailController);
  //server.on("/getEmail", HTTP_GET, getEmailController);
  server.on("/setRegId", HTTP_POST, setRegIdController);
  server.on("/getRegId", HTTP_GET, getRegIdController);
  server.on("/setUtc", HTTP_POST, setUtcController);
  server.on("/getUtc", HTTP_GET, getUtcController);
  server.on("/setTimer", HTTP_POST, setTimerController);
  server.on("/getTimer", HTTP_GET, getTimerController);
  server.on("/setStorm", HTTP_POST, setStormController);
  server.on("/getStorm", HTTP_GET, getStormController);
  server.on("/getTemperatura", HTTP_GET, getTemperaturaController);
  server.on("/modo", HTTP_GET, configuracaoController);
  server.on("/resetDefault", HTTP_GET, resetController);
  server.on("/leds", HTTP_GET, ledsController);
  server.begin();
}



